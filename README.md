# JMS Client Demo
[![pipeline status](https://gitlab.com/lco-utils/jms-client-demo/badges/master/pipeline.svg)](https://gitlab.com/lco-utils/jms-client-demo/commits/master)

A plain implementation of a Java JMS topic client (particularly for ActiveMQ).
