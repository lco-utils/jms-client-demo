package lco.demo.jms;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentSkipListSet;

import static java.util.Collections.unmodifiableSet;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.*;

public class JmsTopicClientTest {
    private static final Logger logger = LoggerFactory.getLogger(JmsTopicClientTest.class);

    private static final String SERVER = "nio://localhost:61616";
    private static final String TOPIC = "good.tv.shows";

    @Test
    public void testPublishSubscribe() throws Exception {
        try (final JmsTopicClient client = new JmsTopicClient(SERVER, TOPIC)) {
            final CompletableFuture<String> futureResult = new CompletableFuture<>();
            client.subscribe(futureResult::complete);

            logger.info("Sending...");
            client.publish("The Wire");

            final String receivedMsg = futureResult.get(3, SECONDS);
            assertNotNull(receivedMsg);
            assertEquals("The Wire", receivedMsg);
        }
    }

    @Test
    public void testPublishSubscribeMultipleMessages() throws Exception {
        final Set<String> messages = unmodifiableSet(Set.of(
                "The Wire",
                "Generation Kill",
                "The Deuce",
                "Band of Brothers"
        ));

        try (final JmsTopicClient client = new JmsTopicClient(SERVER, TOPIC)) {
            final Set<String> receivedMessages = new ConcurrentSkipListSet<>();
            client.subscribe(receivedMessages::add);

            logger.info("Sending...");
            messages.forEach(client::publish);

            Thread.sleep(3 * 1000); // 3 seconds

            assertFalse(receivedMessages.isEmpty());
            assertTrue(receivedMessages.containsAll(messages));
        }
    }

    @Test
    public void testPublishSubscribeObject() throws Exception {
        try (final JmsTopicClient client = new JmsTopicClient(SERVER, TOPIC)) {
            final CompletableFuture<Set<String>> futureResult = new CompletableFuture<>();
            client.subscribe(futureResult::complete, Set.class);

            logger.info("Sending...");
            client.publish(Set.of("The Deuce"));

            final Set<String> receivedMsg = futureResult.get(3, SECONDS);
            assertNotNull(receivedMsg);
            assertEquals("The Deuce", receivedMsg.iterator().next());
        }
    }
}
