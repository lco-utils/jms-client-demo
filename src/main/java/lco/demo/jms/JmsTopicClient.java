package lco.demo.jms;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import static javax.jms.DeliveryMode.NON_PERSISTENT;
import static javax.jms.Session.AUTO_ACKNOWLEDGE;
import static lco.demo.jms.Serializer.deserialize;
import static lco.demo.jms.Serializer.serialize;

public class JmsTopicClient implements AutoCloseable {
    private static final Logger logger = LoggerFactory.getLogger(JmsTopicClient.class);

    private final Set<Connection> activeConnections = new HashSet<>();

    private final ConnectionFactory factory;
    private final ActiveMQTopic topic;

    public JmsTopicClient(final String server, final String topicName) {
        factory = new ActiveMQConnectionFactory(server);
        topic = new ActiveMQTopic(topicName);
    }

    public <T> void publish(final T obj) {
        publish(serialize(obj));
    }

    public void publish(final String text) {
        try (final Connection connection = factory.createConnection();
             final Session session = connection.createSession(false, AUTO_ACKNOWLEDGE);
             final MessageProducer producer = session.createProducer(topic)) {

            producer.setDeliveryMode(NON_PERSISTENT);
            final TextMessage msg = session.createTextMessage(text);
            producer.send(msg);
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    public void subscribe(final Consumer<String> handler) throws Exception {
        subscribe(handler, String.class);
    }

    public <T> void subscribe(final Consumer<T> handler, final Class<T> classOfT) throws Exception {
        final Connection connection = factory.createConnection();
        final Session session = connection.createSession(false, AUTO_ACKNOWLEDGE);
        final MessageConsumer consumer = session.createConsumer(topic);

        consumer.setMessageListener(msg -> {
            try {
                if (msg instanceof TextMessage) {
                    final String text = ((TextMessage) msg).getText();
                    logger.info("Received: " + text);
                    final T obj = String.class.equals(classOfT) ? (T) text : deserialize(text, classOfT);
                    handler.accept(obj);
                } else {
                    logger.warn("Received Unknown message");
                }
            } catch (Exception e) {
                logger.error("Failed to read JMS message: " + e.getMessage(), e);
            }
        });
        connection.start();
        activeConnections.add(connection);
    }

    @Override
    public void close() {
        activeConnections.forEach(c -> {
            try {
                c.close();
            } catch (JMSException ignore) {
            }
        });
    }
}
