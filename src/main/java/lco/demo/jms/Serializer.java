package lco.demo.jms;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Serializer {
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public static <T> String serialize(final T obj) {
        return GSON.toJson(obj);
    }

    public static <T> T deserialize(final String str, final Class<T> classOfT) {
        return GSON.fromJson(str, classOfT);
    }
}
